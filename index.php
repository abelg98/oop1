<?php
require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$animal = new Animal("frog");
$kodok = new Frog("buduk");
echo "Nama Binatang : $animal->name<br>";
echo "Jumlah Kaki : $animal->legs<br>";
echo "Cold Blooded : $animal->cold_blooded<br>";
echo "Suara Lompat : $kodok->jump()<br>";

$animal = new Animal("frog");
$sungokong = new Ape("kera sakti");
echo "Nama Binatang : $animal->name<br>";
echo "Jumlah Kaki : $animal->legs<br>";
echo "Cold Blooded : $animal->cold_blooded<br>";
echo "Suara Lompat : $sungokong->yell()<br>";
